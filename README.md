AV-98

THIS PROJECT HAS MOVED!!!

All further development on AV-98 will happen in the repo at https://git.sr.ht/~solderpunk/AV-98/

Project description, documentation, news, etc. can be found at gemini://zaibatsu.circumlunar.space/~solderpunk/software/av98/

Please DO NOT open issues or pull requests at tildegit.org for this project. Please update links or bookmarks.
